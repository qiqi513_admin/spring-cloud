package com.yz.gateway;

import cn.hutool.jwt.JWTUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@EnableDiscoveryClient
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {


        SpringApplication.run(GatewayApplication.class, args);

        Map<String, Object> map = new HashMap<>();
        map.put("name", "zhangsan");

        String token = JWTUtil.createToken(map, "token".getBytes(StandardCharsets.UTF_8));
        System.out.println("jwt token: ");

        System.out.println(token);
    }


}
