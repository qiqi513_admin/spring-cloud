package com.yz.gateway;

import cn.hutool.jwt.JWTUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

/**
 * 拦截认证
 */
@Slf4j
@Component
public class AuthGatewayFilter  implements GlobalFilter {

    private static final String TOKEN = "token";
    private static final String JWT_KEY = "token";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest request = exchange.getRequest();
        // 获取token
        String auth = request.getHeaders().getFirst(TOKEN);

        try {
            // 校验token
            boolean verify = JWTUtil.verify(auth, JWT_KEY.getBytes(StandardCharsets.UTF_8));
            if(!verify) {
                // 校验失败，返回401
                return unAuth(exchange.getResponse(), "认证失败");
            }

        }catch (Exception e){
            // 解析token失败，返回401
            return unAuth(exchange.getResponse(), "认证失败");
        }

        // 校验通过，放行
        return chain.filter(exchange);
    }



    private Mono<Void> unAuth(ServerHttpResponse resp, String msg) {
        resp.setStatusCode(HttpStatus.UNAUTHORIZED);
        DataBuffer buffer = resp.bufferFactory().wrap(msg.getBytes(StandardCharsets.UTF_8));
        return resp.writeWith(Flux.just(buffer));
    }

}
