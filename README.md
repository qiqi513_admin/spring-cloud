# spring-cloud

**基于Spring Cloud的微服务化开发平台**
```

技术点：
	
	核心框架：Spring Cloud Greenwich.RELEASE、Spring Boot 2.1.3.RELEASE
	安全框架：Spring Security、Oauth2
	持久层：MyBatis、Mybatis_Plus
	网关: Spring Cloud Gateway
	分布式事务: LCN4.0
	缓存: Redis
```

#### 模块

``` 
spring-cloud
│
├── auth -- 授权服务提供[1003]
│
├── common -- 系统公共模块 
│    │
│    ├── common-bom-- 版本管理
│    │
│    ├── common-core -- 公共工具类核心包
│    │
│    ├── common-log -- 日志服务
│    │
│    ├── common-security -- 安全工具类
│    │
│    ├── common-swagger -- Swagger Api文档生成
│    │
│    └── common-transaction -- 事务依赖包
│    
├── config -- 配置中心[1001]
│  
├── eureka -- 服务注册与发现[1000]
│    
├── gateway -- Spring Cloud Gateway网关[1002]
│    
├── modules -- 业务服务模块
│    │
│    ├── upms-api -- 通用用户权限管理系统公共api模块
│    │
│    └── upms -- 通用用户权限管理系统业务处理模块[1004]
│ 
├── visual -- 图形化管理模块
│    │
│    ├── codegen -- 代码生成模块
│    │
│    └── tx-manager -- 分布式事务manager
│ 
```
