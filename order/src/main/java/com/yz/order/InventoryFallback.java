package com.yz.order;

import org.springframework.stereotype.Component;

@Component
public class InventoryFallback implements RemoteInventoryService {
    @Override
    public String decrease(String productId, int quantity) {
        return "仓库服务降级，降级返回";
    }
}
