package com.yz.order;


import lombok.Data;

@Data
public class Order {

    /**
     * 订单ID
     */
    private String orderId;


    /**
     * 地址
     */
    private String address;

    /**
     * 客户ID
     */
    private String customerId;

    /**
     * 产品ID
     */
    private String productId;

    /**
     * 单价
     */
    private double unitPrice;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 订单总价
     */
    private double price;
}
