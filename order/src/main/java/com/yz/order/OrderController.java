package com.yz.order;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.sun.org.apache.xpath.internal.operations.Or;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private final RemoteInventoryService inventoryService;

    /**
     * 订单创建
     */
    @PostMapping(value = "/create")
    @SentinelResource(value = "createOrder", blockHandler = "handleBlock")
    public String create(@RequestBody Order order)  {
        // 订单创建业务逻辑'

        // 调用远程服务，扣减库存
        String decrease = inventoryService.decrease(order.getProductId(), order.getQuantity());

        return "订单创建成功 ： " + order+ " 扣减库存结果：" + decrease;
    }

    // 限流或熔断时的处理方法
    public String handleBlock(String name, BlockException ex) {
        // 当触发限流或熔断时，返回的处理信息
        return "触发限流或熔断，请稍后重试: " + ex.getClass().getSimpleName();
    }

    /**
     * 订单查询
     */
    @GetMapping(value = "/query")
    public String query(@RequestParam("userId") Long userId )  {
        // 订单查询业务逻辑
        return "订购单查询成功 " ;
    }


    /**
     * 订单更新
     */
    @PostMapping(value = "/update")
    public String update(@RequestBody Order order)  {
        // 订单修改业务逻辑
        return "订单修改成功 ：" + order.getAddress();
    }



    /**
     * 订单统计
     */
    @GetMapping(value = "/statistics")
    public String statistics(@RequestParam("productId") String productId )  {
        return "统计数据，产品ID：" + productId + "；统计结果：10";
    }

}
