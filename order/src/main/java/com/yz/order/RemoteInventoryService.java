package com.yz.order;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
@FeignClient(contextId = "RemoteInventoryService", value = "inventory", fallback = InventoryFallback.class)
public interface RemoteInventoryService {


    /**
     * 库存服务接口
     */
    @PostMapping("/inventory/reduce")
    String  decrease(@RequestParam("productId") String productId, @RequestParam("quantity") int quantity);
}
