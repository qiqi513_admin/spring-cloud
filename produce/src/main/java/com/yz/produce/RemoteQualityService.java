package com.yz.produce;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "RemoteQualityService", value = "quality", fallback = QualityFallback.class)
public interface RemoteQualityService {


    /**
     * 产品质量查询
     */
    @PostMapping("/quality/query")
    String  query(@RequestParam("productId") String productId);


    /**
     * 产品质量查询
     */
    @PostMapping("/quality/check")
    String check(@RequestParam("productId") String productId);


}
