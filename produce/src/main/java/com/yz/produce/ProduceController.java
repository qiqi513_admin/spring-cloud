package com.yz.produce;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/produce")
public class ProduceController {


    private final RemoteInventoryService inventoryService;
    private final RemoteOrderService orderService;
    private final RemoteQualityService qualityService;
    private final RemoteDeviceService deviceService;




    /**
     * 指导生成计划
     */
    @PostMapping(value = "/plan")
    public String plan()  {

        String productId = "123456";
        // 查询订单
        String order = orderService.statistics(productId);

        // 查询库存
        String inventory = inventoryService.query(productId);

        // 查询质量
        String quality = qualityService.query(productId);

        // 查询质量
        String queried = deviceService.query();

        return "根据订单状况：" + order + "，库存状况：" + inventory + "，质量状况：" + quality + "，设备状况：" + queried + "，生成生产计划。";
    }

    /**
     * 人员分配
     */
    @PostMapping(value = "/allocation")
    public String allocation(@RequestParam("planId") String planId)  {
        return "人员分配成功。";
    }

    /**
     * 生产进度更新
     */
    @PostMapping(value = "/create")
    public String create(@RequestParam("productId") String productId)  {

        // 调用远程服务，增加库存
        String add = inventoryService.add(productId, 1);

        // 调用远程服务，检查产品质量
        String check = qualityService.check(productId);

        return "产品生产成功 ： " + add + "。质量检查结果：" + check;
    }


    /**
     * 生产进度监控
     */
    @PostMapping(value = "/monitor")
    public String monitor()  {
        return "监控生产进度。30分钟后，如果生产进度未达到预期，则进行人工介入。";
    }


    /**
     * 历史记录查询
     */
    @GetMapping(value = "/history")
    public String history()  {
        return "查询历史生产记录。";
    }


}
