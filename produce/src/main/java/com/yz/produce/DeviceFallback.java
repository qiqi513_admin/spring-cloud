package com.yz.produce;

import org.springframework.stereotype.Component;

@Component
public class DeviceFallback implements RemoteDeviceService {


    @Override
    public String query() {
        return "设备服务降级，降级返回";
    }
}
