package com.yz.produce;


import org.springframework.stereotype.Component;

@Component
public class QualityFallback implements RemoteQualityService {
    @Override
    public String query( String productId) {
        return "质量服务降级，降级返回";
    }

    @Override
    public String check(String product) {
        return "质量服务降级，降级返回";
    }
}
