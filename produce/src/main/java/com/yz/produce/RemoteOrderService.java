package com.yz.produce;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "RemoteOrderService", value = "order", fallback = OrderFallback.class)
public interface RemoteOrderService {


    /**
     * 库存服务接口
     */
    @GetMapping("/order/statistics")
    String  statistics(@RequestParam("productId") String productId  );

}
