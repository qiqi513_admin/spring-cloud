package com.yz.produce;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "RemoteDeviceService", value = "device", fallback = DeviceFallback.class)
public interface RemoteDeviceService {


    /**
     * 库存服务接口
     */
    @PostMapping("/device/query")
    String  query();




}
