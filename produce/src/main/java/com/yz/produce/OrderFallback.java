package com.yz.produce;

import org.springframework.stereotype.Component;

@Component
public class OrderFallback implements RemoteOrderService {


    @Override
    public String statistics(String productId) {
        return "订单服务降级，降级返回";
    }
}
