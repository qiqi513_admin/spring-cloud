package com.yz.produce;

import org.springframework.stereotype.Component;

@Component
public class InventoryFallback implements RemoteInventoryService {
    @Override
    public String add(String productId, int quantity) {
        return "仓库服务降级，降级返回";
    }

    @Override
    public String query(String productId) {
        return "仓库服务降级，降级返回";
    }


}
