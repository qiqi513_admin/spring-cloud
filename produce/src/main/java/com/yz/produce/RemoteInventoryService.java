package com.yz.produce;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "RemoteInventoryService", value = "inventory", fallback = InventoryFallback.class)
public interface RemoteInventoryService {


    /**
     * 库存服务接口
     */
    @PostMapping("/inventory/add")
    String  add(@RequestParam("productId") String productId, @RequestParam("quantity") int quantity);


    /**
     * 库存服务接口
     */
    @GetMapping("/inventory/query")
    String  query(@RequestParam("productId") String productId);

}
