package com.yz.Inventory;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/inventory")
public class InventoryController {

    /**
     * 库存查询
     */
    @GetMapping(value = "/query")
    public String query(@RequestParam("productId") String productId)  {
        return "商品ID "+ productId +"当前库存为：100";
    }

    /**
     * 库存入库
     */
    @PostMapping(value = "/add")
    public String add(@RequestParam("productId") String productId, @RequestParam("quantity") int quantity)  {
        return "库存增加成功 ：产品ID " + productId + "。数量 " + quantity;
    }

    /**
     * 库存出库
     */
    @PostMapping(value = "/reduce")
    public String create(@RequestParam("productId") String productId, @RequestParam("quantity") int quantity)  {
        // 模拟业务异常，触发熔断

        return "库存减少成功 ：产品ID " + productId + "。数量 " + quantity;
    }



    /**
     * 库存调整
     */
    @PostMapping(value = "/transfer")
    public String transfer(@RequestParam("productId") String productId, @RequestParam("quantity") int quantity)  {
        return "库存调整成功 ：产品ID " + productId + "。数量 " + quantity;
    }


    /**
     * 库存查询
     */
    @GetMapping(value = "/history")
    public String history()  {
        return "库存历史记录";
    }


    /**
     * 库存预警设置
     */
    @PostMapping(value = "/setting")
    public String setting(@RequestParam("productId") String productId, @RequestParam("quantity") int quantity)  {
        return "库存预警设置成功 商品ID " + productId + "。预警数量 " + quantity;
    }


}
