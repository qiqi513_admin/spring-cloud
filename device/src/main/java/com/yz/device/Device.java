package com.yz.device;

import lombok.Data;

@Data
public class Device {


    /**
     * 设备ID
     *
     */
    private String deviceId;



    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 设备型号
     */
    private String deviceModel;


    /**
     * 设备版本
     */
    private String deviceVersion;


}
