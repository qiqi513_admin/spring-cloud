package com.yz.device;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/device")
public class DeviceController {


    /**
     * 设备注册
     */
    @PostMapping(value = "/add")
    public String add(@RequestBody Device device)  {
        return "设备注册成功，" + device;
    }



    /**
     * 设备实时数据
     */
    @PostMapping(value = "/realtime")
    public String realtime(@RequestParam("deviceId") String deviceId)  {
        return  deviceId + "设备实时数据：运行正常，电压3.3V，电流100mA，温度30度";
    }

    /**
     * 查询设备运行情况
     */
    @PostMapping(value = "/query")
    public String query()  {
        return "设备运行情况：正常，一天可生产1000000个产品。";
    }


    /**
     * 设备维护
     */
    @PostMapping(value = "/maintenance")
    public String maintenance(@RequestBody DeviceMaintenance device)  {
        return "设备维护成功，" + device;
    }



}
