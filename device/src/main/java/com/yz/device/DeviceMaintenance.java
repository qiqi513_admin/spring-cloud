package com.yz.device;

import lombok.Data;

import java.util.Date;

@Data
public class DeviceMaintenance {

    /**
     * 设备ID
     */
    private String deviceId;

    /**
     * 维护人员ID
     */
    private String userId;

    /**
     * 维护时间
     */
    private String time;

    /**
     * 维护描述
     */
    private String description;
}
