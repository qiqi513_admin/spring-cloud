/*
 Navicat Premium Data Transfer

 Source Server         : docker
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : 10.0.0.33:3306
 Source Schema         : cloud

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 01/03/2019 15:51:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_datasource
-- ----------------------------
DROP TABLE IF EXISTS `gen_datasource`;
CREATE TABLE `gen_datasource`  (
  `datasource_Id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`datasource_Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据源管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for job_execution_log
-- ----------------------------
DROP TABLE IF EXISTS `job_execution_log`;
CREATE TABLE `job_execution_log`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `task_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `hostname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sharding_item` int(11) NOT NULL,
  `execution_source` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `failure_cause` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_success` int(11) NOT NULL,
  `start_time` timestamp(0) NULL DEFAULT NULL,
  `complete_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for job_status_trace_log
-- ----------------------------
DROP TABLE IF EXISTS `job_status_trace_log`;
CREATE TABLE `job_status_trace_log`  (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `original_task_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `task_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `slave_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `source` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `execution_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sharding_item` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `state` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `message` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creation_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务轨迹表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pdman_db_version
-- ----------------------------
DROP TABLE IF EXISTS `pdman_db_version`;
CREATE TABLE `pdman_db_version`  (
  `DB_VERSION` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `VERSION_DESC` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CREATED_TIME` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for upms_dept
-- ----------------------------
DROP TABLE IF EXISTS `upms_dept`;
CREATE TABLE `upms_dept`  (
  `dept_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  `parent_id` int(11) NULL DEFAULT NULL,
  `tenant_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_dept
-- ----------------------------
INSERT INTO `upms_dept` VALUES (1, '山东农信', NULL, '2018-01-22 19:00:23', '2018-09-13 01:46:29', '0', 0, 1);
INSERT INTO `upms_dept` VALUES (2, '沙县国际', NULL, '2018-01-22 19:00:38', '2018-09-13 01:46:30', '0', 0, 1);
INSERT INTO `upms_dept` VALUES (3, '潍坊农信', NULL, '2018-01-22 19:00:44', '2018-09-13 01:46:31', '0', 1, 1);
INSERT INTO `upms_dept` VALUES (4, '高新农信', NULL, '2018-01-22 19:00:52', '2018-10-06 10:41:52', '0', 3, 1);
INSERT INTO `upms_dept` VALUES (5, '院校农信', NULL, '2018-01-22 19:00:57', '2018-10-06 10:42:51', '0', 4, 1);
INSERT INTO `upms_dept` VALUES (6, '潍院农信', NULL, '2018-01-22 19:01:06', '2019-01-09 10:58:18', '1', 5, 1);
INSERT INTO `upms_dept` VALUES (7, '山东沙县', NULL, '2018-01-22 19:01:57', '2018-09-13 01:46:42', '0', 2, 1);
INSERT INTO `upms_dept` VALUES (8, '潍坊沙县', NULL, '2018-01-22 19:02:03', '2018-09-13 01:46:43', '0', 7, 1);
INSERT INTO `upms_dept` VALUES (9, '高新沙县', NULL, '2018-01-22 19:02:14', '2018-09-13 01:46:44', '1', 8, 1);
INSERT INTO `upms_dept` VALUES (10, '租户2', NULL, '2018-11-18 13:27:11', '2018-11-18 13:42:19', '0', 0, 2);
INSERT INTO `upms_dept` VALUES (11, '院校沙县', NULL, '2018-12-10 21:19:26', NULL, '0', 8, 1);

-- ----------------------------
-- Table structure for upms_dept_relation
-- ----------------------------
DROP TABLE IF EXISTS `upms_dept_relation`;
CREATE TABLE `upms_dept_relation`  (
  `ancestor` int(11) NOT NULL COMMENT '祖先节点',
  `descendant` int(11) NOT NULL COMMENT '后代节点',
  PRIMARY KEY (`ancestor`, `descendant`) USING BTREE,
  INDEX `idx1`(`ancestor`) USING BTREE,
  INDEX `idx2`(`descendant`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '部门关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_dept_relation
-- ----------------------------
INSERT INTO `upms_dept_relation` VALUES (1, 1);
INSERT INTO `upms_dept_relation` VALUES (1, 3);
INSERT INTO `upms_dept_relation` VALUES (1, 4);
INSERT INTO `upms_dept_relation` VALUES (1, 5);
INSERT INTO `upms_dept_relation` VALUES (2, 2);
INSERT INTO `upms_dept_relation` VALUES (2, 7);
INSERT INTO `upms_dept_relation` VALUES (2, 8);
INSERT INTO `upms_dept_relation` VALUES (2, 11);
INSERT INTO `upms_dept_relation` VALUES (3, 3);
INSERT INTO `upms_dept_relation` VALUES (3, 4);
INSERT INTO `upms_dept_relation` VALUES (3, 5);
INSERT INTO `upms_dept_relation` VALUES (4, 4);
INSERT INTO `upms_dept_relation` VALUES (4, 5);
INSERT INTO `upms_dept_relation` VALUES (5, 5);
INSERT INTO `upms_dept_relation` VALUES (7, 7);
INSERT INTO `upms_dept_relation` VALUES (7, 8);
INSERT INTO `upms_dept_relation` VALUES (7, 11);
INSERT INTO `upms_dept_relation` VALUES (8, 8);
INSERT INTO `upms_dept_relation` VALUES (8, 11);
INSERT INTO `upms_dept_relation` VALUES (10, 10);
INSERT INTO `upms_dept_relation` VALUES (11, 11);

-- ----------------------------
-- Table structure for upms_dict
-- ----------------------------
DROP TABLE IF EXISTS `upms_dict`;
CREATE TABLE `upms_dict`  (
  `id` int(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据值',
  `label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签名',
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `sort` int(10) NOT NULL COMMENT '排序（升序）',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属租户',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `upms_dict_value`(`value`) USING BTREE,
  INDEX `upms_dict_label`(`label`) USING BTREE,
  INDEX `upms_dict_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_dict
-- ----------------------------
INSERT INTO `upms_dict` VALUES (1, '9', '异常', 'log_type', '日志异常', 1, '2018-07-09 06:16:14', '2018-11-24 07:25:11', '日志异常', '0', 1);
INSERT INTO `upms_dict` VALUES (2, '0', '正常', 'log_type', '正常', 0, '2018-07-09 06:15:40', '2018-11-24 07:25:14', '正常', '0', 0);
INSERT INTO `upms_dict` VALUES (3, 'WX', '微信', 'social_type', '微信登录', 0, '2018-08-16 14:01:45', '2018-11-24 07:25:16', '微信登录', '0', 0);
INSERT INTO `upms_dict` VALUES (4, 'QQ', 'QQ', 'social_type', 'QQ登录', 1, '2018-07-09 06:15:40', '2018-11-24 07:25:18', 'QQ登录', '0', 0);
INSERT INTO `upms_dict` VALUES (5, '0', '正常', 'log_type', '日志类型', 0, '2018-09-30 02:33:53', '2018-11-24 07:25:20', '日志正常', '0', 1);
INSERT INTO `upms_dict` VALUES (6, '0', '未提交', 'leave_status', '请假状态', 0, '2018-09-30 02:34:45', '2018-11-24 07:25:23', '请假状态', '0', 1);
INSERT INTO `upms_dict` VALUES (7, '1', '审批中', 'leave_status', '请假状态', 1, '2018-09-30 02:35:16', '2018-11-24 07:25:25', '请假状态', '0', 1);
INSERT INTO `upms_dict` VALUES (8, '2', '完成', 'leave_status', '请假状态', 2, '2018-09-30 02:35:58', '2018-11-24 07:25:28', '请假状态', '0', 1);
INSERT INTO `upms_dict` VALUES (9, '9', '驳回', 'leave_status', '请假状态', 9, '2018-09-30 02:36:31', '2018-11-24 07:25:31', '请假状态', '0', 1);

-- ----------------------------
-- Table structure for upms_log
-- ----------------------------
DROP TABLE IF EXISTS `upms_log`;
CREATE TABLE `upms_log`  (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '日志标题',
  `service_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务ID',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remote_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作方式',
  `params` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作提交的数据',
  `time` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '执行时间',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记',
  `exception` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常信息',
  `tenant_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属租户',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `upms_log_create_by`(`create_by`) USING BTREE,
  INDEX `upms_log_request_uri`(`request_uri`) USING BTREE,
  INDEX `upms_log_type`(`type`) USING BTREE,
  INDEX `upms_log_create_date`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_log
-- ----------------------------
INSERT INTO `upms_log` VALUES (1, '0', '更新用户信息', 'test', 'admin', '2019-01-31 09:20:31', NULL, '10.0.0.203', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '/upmsUser', 'PUT', '', '130', '0', NULL, 0);

-- ----------------------------
-- Table structure for upms_menu
-- ----------------------------
DROP TABLE IF EXISTS `upms_menu`;
CREATE TABLE `upms_menu`  (
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `permission` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单权限标识',
  `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端URL',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父菜单ID',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `component` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'VUE页面',
  `sort` int(11) NULL DEFAULT 1 COMMENT '排序值',
  `keep_alive` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '0-开启，1- 关闭',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单类型 （0菜单 1按钮）',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '逻辑删除标记(0--正常 1--删除)',
  `tenant_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_menu
-- ----------------------------
INSERT INTO `upms_menu` VALUES (1000, '权限管理', NULL, '/upms', -1, 'icon-quanxianguanli', 'Layout', 0, '0', '0', '2018-09-28 08:29:53', '2018-09-28 08:53:01', '0', 1);
INSERT INTO `upms_menu` VALUES (1100, '用户管理', NULL, 'user', 1000, 'icon-yonghuguanli', 'views/upms/user/index', 1, '0', '0', '2017-11-02 22:24:37', '2019-01-22 09:34:14', '0', 1);
INSERT INTO `upms_menu` VALUES (1101, '用户新增', 'upms_user_add', NULL, 1100, NULL, NULL, NULL, '0', '1', '2017-11-08 09:52:09', '2018-09-28 09:06:34', '0', 1);
INSERT INTO `upms_menu` VALUES (1102, '用户修改', 'upms_user_edit', NULL, 1100, NULL, NULL, NULL, '0', '1', '2017-11-08 09:52:48', '2018-09-28 09:06:37', '0', 1);
INSERT INTO `upms_menu` VALUES (1103, '用户删除', 'upms_user_del', NULL, 1100, NULL, NULL, NULL, '0', '1', '2017-11-08 09:54:01', '2018-09-28 09:06:42', '0', 1);
INSERT INTO `upms_menu` VALUES (1200, '菜单管理', NULL, 'menu', 1000, 'icon-caidanguanli', 'views/upms/menu/index', 2, '0', '0', '2017-11-08 09:57:27', '2019-01-25 07:03:14', '0', 1);
INSERT INTO `upms_menu` VALUES (1201, '菜单新增', 'upms_menu_add', NULL, 1200, NULL, NULL, NULL, '0', '1', '2017-11-08 10:15:53', '2018-09-28 09:07:16', '0', 1);
INSERT INTO `upms_menu` VALUES (1202, '菜单修改', 'upms_menu_edit', NULL, 1200, NULL, NULL, NULL, '0', '1', '2017-11-08 10:16:23', '2018-09-28 09:07:18', '0', 1);
INSERT INTO `upms_menu` VALUES (1203, '菜单删除', 'upms_menu_del', NULL, 1200, NULL, NULL, NULL, '0', '1', '2017-11-08 10:16:43', '2018-09-28 09:07:22', '0', 1);
INSERT INTO `upms_menu` VALUES (1300, '角色管理', NULL, 'role', 1000, 'icon-jiaoseguanli', 'views/upms/role/index', 3, '0', '0', '2017-11-08 10:13:37', '2018-09-28 09:00:48', '0', 1);
INSERT INTO `upms_menu` VALUES (1301, '角色新增', 'upms_role_add', NULL, 1300, NULL, NULL, NULL, '0', '1', '2017-11-08 10:14:18', '2018-09-28 09:07:46', '0', 1);
INSERT INTO `upms_menu` VALUES (1302, '角色修改', 'upms_role_edit', NULL, 1300, NULL, NULL, NULL, '0', '1', '2017-11-08 10:14:41', '2018-09-28 09:07:49', '0', 1);
INSERT INTO `upms_menu` VALUES (1303, '角色删除', 'upms_role_del', NULL, 1300, NULL, NULL, NULL, '0', '1', '2017-11-08 10:14:59', '2018-09-28 09:07:53', '0', 1);
INSERT INTO `upms_menu` VALUES (1304, '分配权限', 'upms_role_perm', NULL, 1300, NULL, NULL, NULL, '0', '1', '2018-04-20 07:22:55', '2018-09-28 09:13:23', '0', 1);
INSERT INTO `upms_menu` VALUES (1400, '部门管理', NULL, 'dept', 1000, 'icon-web-icon-', 'views/upms/dept/index', 4, '0', '0', '2018-01-20 13:17:19', '2018-12-09 16:35:12', '0', 1);
INSERT INTO `upms_menu` VALUES (1401, '部门新增', 'upms_dept_add', NULL, 1400, NULL, NULL, NULL, '0', '1', '2018-01-20 14:56:16', '2018-09-28 09:08:13', '0', 1);
INSERT INTO `upms_menu` VALUES (1402, '部门修改', 'upms_dept_edit', NULL, 1400, NULL, NULL, NULL, '0', '1', '2018-01-20 14:56:59', '2018-09-28 09:08:16', '0', 1);
INSERT INTO `upms_menu` VALUES (1403, '部门删除', 'upms_dept_del', NULL, 1400, NULL, NULL, NULL, '0', '1', '2018-01-20 14:57:28', '2018-09-28 09:08:18', '0', 1);
INSERT INTO `upms_menu` VALUES (2000, '系统管理', NULL, '/admin', -1, 'icon-xitongguanli', 'Layout', 1, '0', '0', '2017-11-07 20:56:00', '2018-09-28 08:53:18', '0', 1);
INSERT INTO `upms_menu` VALUES (2100, '日志管理', NULL, 'log', 2000, 'icon-rizhiguanli', 'views/upms/log/index', 5, '0', '0', '2017-11-20 14:06:22', '2018-09-28 09:01:52', '0', 1);
INSERT INTO `upms_menu` VALUES (2101, '日志删除', 'upms_log_del', NULL, 2100, NULL, NULL, NULL, '0', '1', '2017-11-20 20:37:37', '2018-09-28 09:08:44', '0', 1);
INSERT INTO `upms_menu` VALUES (2200, '字典管理', NULL, 'dict', 2000, 'icon-navicon-zdgl', 'views/upms/dict/index', 6, '0', '0', '2017-11-29 11:30:52', '2018-09-28 09:01:47', '0', 1);
INSERT INTO `upms_menu` VALUES (2201, '字典删除', 'upms_dict_del', NULL, 2200, NULL, NULL, NULL, '0', '1', '2017-11-29 11:30:11', '2018-09-28 09:09:10', '0', 1);
INSERT INTO `upms_menu` VALUES (2202, '字典新增', 'upms_dict_add', NULL, 2200, NULL, NULL, NULL, '0', '1', '2018-05-11 22:34:55', '2018-09-28 09:09:12', '0', 1);
INSERT INTO `upms_menu` VALUES (2203, '字典修改', 'upms_dict_edit', NULL, 2200, NULL, NULL, NULL, '0', '1', '2018-05-11 22:36:03', '2018-09-28 09:09:16', '0', 1);
INSERT INTO `upms_menu` VALUES (2300, '代码生成', '', 'gen', 2000, 'icon-weibiaoti46', 'views/gen/index', 8, '0', '0', '2018-01-20 13:17:19', '2018-11-24 05:21:01', '0', 1);
INSERT INTO `upms_menu` VALUES (2400, '终端管理', '', 'client', 2000, 'icon-shouji', 'views/upms/client/index', 9, '0', '0', '2018-01-20 13:17:19', '2018-09-28 09:01:43', '0', 1);
INSERT INTO `upms_menu` VALUES (2401, '客户端新增', 'upms_client_add', NULL, 2400, '1', NULL, NULL, '0', '1', '2018-05-15 21:35:18', '2018-09-28 09:10:25', '0', 1);
INSERT INTO `upms_menu` VALUES (2402, '客户端修改', 'upms_client_edit', NULL, 2400, NULL, NULL, NULL, '0', '1', '2018-05-15 21:37:06', '2018-09-28 09:10:27', '0', 1);
INSERT INTO `upms_menu` VALUES (2403, '客户端删除', 'upms_client_del', NULL, 2400, NULL, NULL, NULL, '0', '1', '2018-05-15 21:39:16', '2018-09-28 09:10:30', '0', 1);
INSERT INTO `upms_menu` VALUES (2500, '密钥管理', '', 'social', 2000, 'icon-miyue', 'views/upms/social/index', 10, '0', '0', '2018-01-20 13:17:19', '2018-09-28 09:01:41', '0', 1);
INSERT INTO `upms_menu` VALUES (2501, '密钥新增', 'generator_upmssocialdetails_add', NULL, 2500, '1', NULL, 0, '0', '1', '2018-05-15 21:35:18', '2018-09-28 09:11:02', '0', 1);
INSERT INTO `upms_menu` VALUES (2502, '密钥修改', 'generator_upmssocialdetails_edit', NULL, 2500, '1', NULL, 1, '0', '1', '2018-05-15 21:35:18', '2018-09-28 09:11:04', '0', 1);
INSERT INTO `upms_menu` VALUES (2503, '密钥删除', 'generator_upmssocialdetails_del', NULL, 2500, '1', NULL, 2, '0', '1', '2018-05-15 21:35:18', '2018-09-28 09:11:06', '0', 1);
INSERT INTO `upms_menu` VALUES (2600, '令牌管理', NULL, 'token', 2000, 'icon-denglvlingpai', 'views/upms/token/index', 11, '0', '0', '2018-09-04 05:58:41', '2018-09-28 09:01:38', '0', 1);
INSERT INTO `upms_menu` VALUES (2601, '令牌删除', 'upms_token_del', NULL, 2600, NULL, NULL, 1, '0', '1', '2018-09-04 05:59:50', '2018-09-28 09:11:24', '0', 1);
INSERT INTO `upms_menu` VALUES (2700, '动态路由', NULL, 'route', 2000, 'icon-luyou', 'views/upms/route/index', 12, '0', '0', '2018-09-04 05:58:41', '2018-09-28 09:01:38', '0', 1);
INSERT INTO `upms_menu` VALUES (3000, '系统监控', NULL, '/daemon', -1, 'icon-msnui-supervise', 'Layout', 2, '0', '0', '2018-07-27 01:13:21', '2018-09-28 08:53:24', '0', 1);
INSERT INTO `upms_menu` VALUES (3100, '服务监控', NULL, 'http://139.224.200.249:15001', 3000, 'icon-server', NULL, 0, '0', '0', '2018-06-26 10:50:32', '2018-12-11 17:17:07', '0', 1);
INSERT INTO `upms_menu` VALUES (3200, '接口文档', NULL, 'http://10.0.0.203:1002/swagger-ui.html', 3000, 'icon-wendang', NULL, 1, '0', '0', '2018-06-26 10:50:32', '2018-09-28 09:05:16', '0', 1);
INSERT INTO `upms_menu` VALUES (3300, '事务监控', NULL, 'tx', 3000, 'icon-gtsquanjushiwufuwuGTS', 'views/tx/index', 5, '0', '0', '2018-08-19 11:02:39', '2018-11-23 14:25:08', '0', 1);
INSERT INTO `upms_menu` VALUES (3400, '在线事务', NULL, 'model', 3000, 'icon-online', 'views/tx/model', 6, '0', '0', '2018-08-19 11:32:04', '2018-11-23 14:25:14', '0', 1);
INSERT INTO `upms_menu` VALUES (3500, '任务监控', NULL, 'http://139.224.200.249:8899', 3000, 'icon-msnui-supervise', NULL, 7, '0', '0', '2018-06-26 10:50:32', '2018-09-28 09:03:33', '0', 1);
INSERT INTO `upms_menu` VALUES (3600, '任务轨迹', '', 'status-trace-log', 3000, 'icon-guiji', 'views/daemon/status-trace-log/index', 8, '0', '0', '2018-01-20 13:17:19', '2018-11-24 06:34:52', '0', 1);
INSERT INTO `upms_menu` VALUES (3601, '删除任务轨迹', 'daemon_status_trace_log_del', NULL, 3600, '1', NULL, 2, '0', '1', '2018-05-15 21:35:18', '2018-11-24 06:37:31', '0', 1);
INSERT INTO `upms_menu` VALUES (3700, '调用拓扑', NULL, 'http://139.224.200.249:8081', 3000, 'icon-line', NULL, 10, '0', '0', '2018-01-25 11:08:52', '2018-09-28 09:04:29', '0', 1);
INSERT INTO `upms_menu` VALUES (3800, '缓存状态', NULL, 'http://139.224.200.249:8585', 3000, 'icon-qingchuhuancun', NULL, 12, '0', '0', '2018-01-23 10:56:11', '2018-09-28 09:13:56', '0', 1);
INSERT INTO `upms_menu` VALUES (3900, '任务日志', '', 'execution-log', 3000, 'icon-guiji', 'views/daemon/execution-log/index', 8, '0', '0', '2018-01-20 13:17:19', '2018-11-24 06:34:52', '0', 1);
INSERT INTO `upms_menu` VALUES (3901, '删除日志', 'daemon_execution_log_del', NULL, 3900, '1', NULL, 2, '0', '1', '2018-05-15 21:35:18', '2018-11-24 06:39:03', '0', 1);

-- ----------------------------
-- Table structure for upms_oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `upms_oauth_client_details`;
CREATE TABLE `upms_oauth_client_details`  (
  `client_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resource_ids` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `client_secret` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `scope` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `authorities` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `autoapprove` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tenant_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属租户',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '终端信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_oauth_client_details
-- ----------------------------
INSERT INTO `upms_oauth_client_details` VALUES ('app', NULL, 'app', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', 1);
INSERT INTO `upms_oauth_client_details` VALUES ('codegen', NULL, 'codegen', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', 1);
INSERT INTO `upms_oauth_client_details` VALUES ('qiqi', NULL, 'qiqi', 'server', 'password,refresh_token,authorization_code', 'http://localhost:4040/sso1/login,http://localhost:4041/sso1/login', NULL, NULL, NULL, NULL, 'true', 1);
INSERT INTO `upms_oauth_client_details` VALUES ('test', NULL, 'test', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', 1);
INSERT INTO `upms_oauth_client_details` VALUES ('upms', NULL, 'upms', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', 1);

-- ----------------------------
-- Table structure for upms_role
-- ----------------------------
DROP TABLE IF EXISTS `upms_role`;
CREATE TABLE `upms_role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `role_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `role_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `ds_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '2' COMMENT '数据权限类型',
  `ds_scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '数据权限范围',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
  `tenant_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `role_idx1_role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_role
-- ----------------------------
INSERT INTO `upms_role` VALUES (1, '管理员', 'ROLE_ADMIN', '管理员', '2', '2', '2017-10-29 15:45:51', '2018-12-26 14:09:11', '0', 1);
INSERT INTO `upms_role` VALUES (2, 'ROLE_CQQ', 'ROLE_CQQ', 'ROLE_CQQ', '2', NULL, '2018-11-11 19:42:26', '2018-12-26 14:09:07', '0', 2);

-- ----------------------------
-- Table structure for upms_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `upms_role_menu`;
CREATE TABLE `upms_role_menu`  (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_role_menu
-- ----------------------------
INSERT INTO `upms_role_menu` VALUES (1, 1000);
INSERT INTO `upms_role_menu` VALUES (1, 1100);
INSERT INTO `upms_role_menu` VALUES (1, 1101);
INSERT INTO `upms_role_menu` VALUES (1, 1102);
INSERT INTO `upms_role_menu` VALUES (1, 1103);
INSERT INTO `upms_role_menu` VALUES (1, 1200);
INSERT INTO `upms_role_menu` VALUES (1, 1201);
INSERT INTO `upms_role_menu` VALUES (1, 1202);
INSERT INTO `upms_role_menu` VALUES (1, 1203);
INSERT INTO `upms_role_menu` VALUES (1, 1300);
INSERT INTO `upms_role_menu` VALUES (1, 1301);
INSERT INTO `upms_role_menu` VALUES (1, 1302);
INSERT INTO `upms_role_menu` VALUES (1, 1303);
INSERT INTO `upms_role_menu` VALUES (1, 1304);
INSERT INTO `upms_role_menu` VALUES (1, 1400);
INSERT INTO `upms_role_menu` VALUES (1, 1401);
INSERT INTO `upms_role_menu` VALUES (1, 1402);
INSERT INTO `upms_role_menu` VALUES (1, 1403);
INSERT INTO `upms_role_menu` VALUES (1, 2000);
INSERT INTO `upms_role_menu` VALUES (1, 2100);
INSERT INTO `upms_role_menu` VALUES (1, 2101);
INSERT INTO `upms_role_menu` VALUES (1, 2200);
INSERT INTO `upms_role_menu` VALUES (1, 2201);
INSERT INTO `upms_role_menu` VALUES (1, 2202);
INSERT INTO `upms_role_menu` VALUES (1, 2203);
INSERT INTO `upms_role_menu` VALUES (1, 2300);
INSERT INTO `upms_role_menu` VALUES (1, 2400);
INSERT INTO `upms_role_menu` VALUES (1, 2401);
INSERT INTO `upms_role_menu` VALUES (1, 2402);
INSERT INTO `upms_role_menu` VALUES (1, 2403);
INSERT INTO `upms_role_menu` VALUES (1, 2500);
INSERT INTO `upms_role_menu` VALUES (1, 2501);
INSERT INTO `upms_role_menu` VALUES (1, 2502);
INSERT INTO `upms_role_menu` VALUES (1, 2503);
INSERT INTO `upms_role_menu` VALUES (1, 2600);
INSERT INTO `upms_role_menu` VALUES (1, 2601);
INSERT INTO `upms_role_menu` VALUES (1, 2700);
INSERT INTO `upms_role_menu` VALUES (1, 3000);
INSERT INTO `upms_role_menu` VALUES (1, 3100);
INSERT INTO `upms_role_menu` VALUES (1, 3200);
INSERT INTO `upms_role_menu` VALUES (1, 3300);
INSERT INTO `upms_role_menu` VALUES (1, 3400);
INSERT INTO `upms_role_menu` VALUES (1, 3500);
INSERT INTO `upms_role_menu` VALUES (1, 3600);
INSERT INTO `upms_role_menu` VALUES (1, 3601);
INSERT INTO `upms_role_menu` VALUES (1, 3700);
INSERT INTO `upms_role_menu` VALUES (1, 3800);
INSERT INTO `upms_role_menu` VALUES (1, 4000);
INSERT INTO `upms_role_menu` VALUES (1, 4100);
INSERT INTO `upms_role_menu` VALUES (1, 4101);
INSERT INTO `upms_role_menu` VALUES (1, 4200);
INSERT INTO `upms_role_menu` VALUES (1, 4201);
INSERT INTO `upms_role_menu` VALUES (1, 4300);
INSERT INTO `upms_role_menu` VALUES (1, 4301);
INSERT INTO `upms_role_menu` VALUES (1, 4302);
INSERT INTO `upms_role_menu` VALUES (1, 4303);
INSERT INTO `upms_role_menu` VALUES (1, 4400);
INSERT INTO `upms_role_menu` VALUES (1, 4401);
INSERT INTO `upms_role_menu` VALUES (2, 1000);
INSERT INTO `upms_role_menu` VALUES (2, 1100);
INSERT INTO `upms_role_menu` VALUES (2, 1101);
INSERT INTO `upms_role_menu` VALUES (2, 1102);
INSERT INTO `upms_role_menu` VALUES (2, 1103);
INSERT INTO `upms_role_menu` VALUES (2, 1200);
INSERT INTO `upms_role_menu` VALUES (2, 1201);
INSERT INTO `upms_role_menu` VALUES (2, 1202);
INSERT INTO `upms_role_menu` VALUES (2, 1203);
INSERT INTO `upms_role_menu` VALUES (2, 1300);
INSERT INTO `upms_role_menu` VALUES (2, 1301);
INSERT INTO `upms_role_menu` VALUES (2, 1302);
INSERT INTO `upms_role_menu` VALUES (2, 1303);
INSERT INTO `upms_role_menu` VALUES (2, 1304);
INSERT INTO `upms_role_menu` VALUES (2, 1400);
INSERT INTO `upms_role_menu` VALUES (2, 1401);
INSERT INTO `upms_role_menu` VALUES (2, 1402);
INSERT INTO `upms_role_menu` VALUES (2, 1403);

-- ----------------------------
-- Table structure for upms_route_conf
-- ----------------------------
DROP TABLE IF EXISTS `upms_route_conf`;
CREATE TABLE `upms_route_conf`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `route_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由名称',
  `route_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '路由ID',
  `predicates` json NULL COMMENT '断言',
  `filters` json NULL COMMENT '过滤器',
  `uri` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `order` int(2) NULL DEFAULT 0 COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '路由配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_route_conf
-- ----------------------------
INSERT INTO `upms_route_conf` VALUES (1, '工作流管理模块', 'pigx-activiti', '[{\"args\": {\"_genkey_0\": \"/act/**\"}, \"name\": \"Path\"}]', '[]', 'lb://pigx-activiti', 0, '2018-11-28 15:56:10', '2018-11-28 15:56:25', '0');
INSERT INTO `upms_route_conf` VALUES (2, '认证中心', 'pigx-auth', '[{\"args\": {\"_genkey_0\": \"/auth/**\"}, \"name\": \"Path\"}]', '[{\"args\": {}, \"name\": \"ValidateCodeGatewayFilter\"}, {\"args\": {}, \"name\": \"PasswordDecoderFilter\"}]', 'lb://pigx-auth', 0, '2018-11-28 15:56:10', '2018-11-28 15:56:29', '0');
INSERT INTO `upms_route_conf` VALUES (3, '代码生成模块', 'pigx-codegen', '[{\"args\": {\"_genkey_0\": \"/gen/**\"}, \"name\": \"Path\"}]', '[]', 'lb://pigx-codegen', 0, '2018-11-28 15:56:10', '2018-11-28 15:56:32', '0');
INSERT INTO `upms_route_conf` VALUES (4, '定时任务模块', 'pigx-daemon', '[{\"args\": {\"_genkey_0\": \"/daemon/**\"}, \"name\": \"Path\"}]', '[]', 'lb://pigx-daemon', 0, '2018-11-28 15:56:10', '2018-11-28 15:56:34', '0');
INSERT INTO `upms_route_conf` VALUES (5, '分布式事务模块', 'pigx-tx-manager', '[{\"args\": {\"_genkey_0\": \"/tx/**\"}, \"name\": \"Path\"}]', '[]', 'lb://pigx-tx-manager', 0, '2018-11-28 15:56:10', '2018-11-28 15:56:36', '0');
INSERT INTO `upms_route_conf` VALUES (6, '通用权限模块', 'pigx-upms-biz', '[{\"args\": {\"_genkey_0\": \"/admin/**\"}, \"name\": \"Path\"}]', '[{\"args\": {\"key-resolver\": \"#{@remoteAddrKeyResolver}\", \"redis-rate-limiter.burstCapacity\": \"20\", \"redis-rate-limiter.replenishRate\": \"10\"}, \"name\": \"RequestRateLimiter\"}, {\"args\": {\"name\": \"default\", \"fallbackUri\": \"forward:/fallback\"}, \"name\": \"Hystrix\"}]', 'lb://pigx-upms-biz', 0, '2018-11-28 15:56:10', '2018-11-28 15:56:39', '0');

-- ----------------------------
-- Table structure for upms_social_details
-- ----------------------------
DROP TABLE IF EXISTS `upms_social_details`;
CREATE TABLE `upms_social_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `app_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'appid',
  `app_secret` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'app_secret',
  `redirect_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回调地址',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `del_flag` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属租户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统社交登录账号表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_social_details
-- ----------------------------
INSERT INTO `upms_social_details` VALUES (1, 'WX', '微信互联参数', 'wxd1678d3f83b1d83a', '6ddb043f94da5d2172926abe8533504f', 'daoweicloud.com', '2018-08-16 14:24:25', '2018-12-04 11:50:57', '0', 1);

-- ----------------------------
-- Table structure for upms_user
-- ----------------------------
DROP TABLE IF EXISTS `upms_user`;
CREATE TABLE `upms_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '随机盐',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '简介',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '头像',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `lock_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-正常，9-锁定',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-正常，1-删除',
  `wx_openid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '微信openid',
  `qq_openid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'QQ openid',
  `tenant_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属租户',
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `user_wx_openid`(`wx_openid`) USING BTREE,
  INDEX `user_qq_openid`(`qq_openid`) USING BTREE,
  INDEX `user_idx1_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_user
-- ----------------------------
INSERT INTO `upms_user` VALUES (1, 'admin', '$2a$10$QOfWxxFyAMmEEmnuw9UI/..1s4B4eF/u9PzE2ZaGO.ij9YfmcUy.u', NULL, '15172490104', 'lengleng-oxjkuoy361p6x5snlogo.png', 1, '2018-04-20 07:15:18', '2019-01-31 17:08:32', '0', '0', 'o_0FT0uyg_H1vVy2H0JpSwlVGhWQ', NULL, 1);
INSERT INTO `upms_user` VALUES (2, 'admin', '$2a$10$RpFJjxYiXdEsAGnWp/8fsOetMuOON96Ntk/Ym2M/RKRyU0GZseaDC', NULL, '17034642887', NULL, 10, '2018-04-20 07:15:18', '2019-01-09 11:06:24', '0', '0', 'o_0FT0uyg_H1vVy2H0JpSwlVGhWQ', NULL, 2);
INSERT INTO `upms_user` VALUES (3, 'qqq', '$2a$10$btLqv.XyDTQkZa6KVXkp/OeFvemXFOFY70sW6B0rFDdTFNZikPGEG', NULL, '12345678915', NULL, 6, '2019-01-31 16:38:23', '2019-01-31 17:20:30', '0', '0', NULL, NULL, 0);

-- ----------------------------
-- Table structure for upms_user_role
-- ----------------------------
DROP TABLE IF EXISTS `upms_user_role`;
CREATE TABLE `upms_user_role`  (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`link_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  CONSTRAINT `upms_user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `upms_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `upms_user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `upms_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_user_role
-- ----------------------------
INSERT INTO `upms_user_role` VALUES (2, 2, 2);
INSERT INTO `upms_user_role` VALUES (8, 1, 1);
INSERT INTO `upms_user_role` VALUES (9, 3, 1);

SET FOREIGN_KEY_CHECKS = 1;
