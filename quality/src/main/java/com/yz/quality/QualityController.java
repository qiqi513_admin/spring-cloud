package com.yz.quality;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/quality")
public class QualityController {


    private final RemoteProduceService produceService;

    /**
     * 产品质量定义
     */
    @PostMapping(value = "/setting")
    public String setting(@RequestParam("product") String product,@RequestParam("standard") String standard)  {
        return "产品：" + product + "的质量定义:"+standard;
    }

    /**
     * 检测计划
     */
    @PostMapping(value = "/plan")
    public String plan()  {
        return "产品质量检测计划";
    }



    /**
     * 产品质量检测
     */
    @PostMapping(value = "/check")
    public String check(@RequestParam("productId") String productId)  {
        return productId + "产品质量检测结果 ： 95%";
    }


    /**
     * 产品质量分析
     */
    @PostMapping(value = "/analysis")
    public String analysis(@RequestParam("productId") String productId)  {

        return "产品质量分析：" + productId;
    }


    /**
     * 查询产品质量分析报告
     */
    @PostMapping(value = "/query")
    public String query(@RequestParam("productId") String productId)  {

        return "商品Id："  + productId + "的产品质量分析报告：100%优";
    }



}
