package com.yz.quality;

import org.springframework.stereotype.Component;

@Component
public class ProduceFallback implements RemoteProduceService {
    @Override
    public String add(String productId, int quantity) {
        return "产品服务降级，降级返回";
    }

    @Override
    public String query() {
        return "产品服务降级，降级返回";
    }
}
