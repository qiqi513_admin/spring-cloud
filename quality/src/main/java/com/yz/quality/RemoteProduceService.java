package com.yz.quality;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "RemoteInventoryService", value = "inventory", fallback = ProduceFallback.class)
public interface RemoteProduceService {


    /**
     * 库存服务接口
     */
    @PostMapping("/inventory/add")
    String  add(@RequestParam("productId") String productId, @RequestParam("quantity") int quantity);


    /**
     * 库存服务接口
     */
    @PostMapping("/inventory/query")
    String  query();

}
